The solution is broken down into three Projects

* Common

    Contains shared plain objects

* HipChatService

    Contains the web service code

* HipChatServiceTests

    Contains tests against the web service code

If I had more time, I would make assumptions about the Links such as supporting English language sites only and implement a rough version that loads the page in memory and parse the title from the html. 

Other missing items
*    Logging
*    Telemetry
*    Sanitizing user input since it is returned back to client as is.