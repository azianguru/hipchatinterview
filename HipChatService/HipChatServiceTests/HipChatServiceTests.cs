﻿using System;
using HipChatService;
using HipChatService.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HipChatServiceTests
{
    /// <summary>
    /// Unittests that exercise the <see cref="MessageService"/> API.
    /// </summary>
    [TestClass]
    public class HipChatServiceTests
    {
        /// <summary>
        /// Confirms that the keepalive endpoint returns the expected fixed string.
        /// </summary>
        [TestMethod]
        public void KeepaliveTest()
        {
            var target = new MessageService();
            Assert.AreEqual("Ok", target.Keepalive());
        }

        /// <summary>
        /// Validates that the ParseMentions method returns correct mentions in chat message.
        /// </summary>
        [TestMethod]
        public void ParseMentionsTest()
        {
            var target = new MessageService();
            var attr = new ChatMessageAttributes();

            // Empty message returns empty attributes.
            {
                string message = "";
                target.ParseMentions(message, attr);
                Assert.AreEqual(0, attr.Mentions.Count);
            }

            // No mentions
            {
                string message = "A quick brown fox jumped over the lazy dog.";
                target.ParseMentions(message, attr);
                Assert.AreEqual(0, attr.Mentions.Count);
            }

            // Mention only
            {
                string message = "@user";
                attr = target.GetContext(message);
                Assert.AreEqual(1, attr.Mentions.Count);
                Assert.AreEqual("user", attr.Mentions[0]);
            }

            // Multiple mentions
            {
                string message = "hey @user @user2 lunch?";
                target.ParseMentions(message, attr);
                Assert.AreEqual(2, attr.Mentions.Count);
                Assert.AreEqual("user", attr.Mentions[0]);
                Assert.AreEqual("user2", attr.Mentions[1]);
            }

            // Mention at beginning of message.
            {
                string message = "@user done yet?";
                target.ParseMentions(message, attr);
                Assert.AreEqual(1, attr.Mentions.Count);
                Assert.AreEqual("user", attr.Mentions[0]);
            }

            // Mention at end of message.
            {
                string message = "I'm waiting on @user";
                target.ParseMentions(message, attr);
                Assert.AreEqual(1, attr.Mentions.Count);
                Assert.AreEqual("user", attr.Mentions[0]);
            }

            // Mention contains word characters.
            {
                string message = "@as123blu_h is missing...";
                target.ParseMentions(message, attr);
                Assert.AreEqual(1, attr.Mentions.Count);
                Assert.AreEqual("as123blu_h", attr.Mentions[0]);
            }
        }

        /// <summary>
        /// Validates that the ParseEmoticons method returns correct emoticons in chat message.
        /// </summary>
        public void ParseEmoticonsTest()
        {
            var target = new MessageService();
            var attr = new ChatMessageAttributes();

            // Empty message returns empty attributes.
            {
                string message = "";
                target.ParseEmoticons(message, attr);
                Assert.AreEqual(0, attr.Emoticons.Count);
            }

            // No emoticons
            {
                string message = "A quick brown fox jumped over the lazy dog.";
                target.ParseEmoticons(message, attr);
                Assert.AreEqual(0, attr.Emoticons.Count);
            }

            // Emoticon only
            {
                string message = "(success)";
                attr = target.GetContext(message);
                Assert.AreEqual(1, attr.Emoticons.Count);
                Assert.AreEqual("success", attr.Emoticons[0]);
            }

            // Multiple emoticons
            {
                string message = "woo (success) (bamboo) yay?";
                target.ParseEmoticons(message, attr);
                Assert.AreEqual(2, attr.Emoticons.Count);
                Assert.AreEqual("success", attr.Emoticons[0]);
                Assert.AreEqual("bamboo", attr.Emoticons[1]);
            }

            // Emoticon at beginning of message.
            {
                string message = "(fail) whale?";
                target.ParseEmoticons(message, attr);
                Assert.AreEqual(1, attr.Emoticons.Count);
                Assert.AreEqual("fail", attr.Emoticons[0]);
            }

            // Emoticon at end of message.
            {
                string message = "I'm waiting for (boom)";
                target.ParseEmoticons(message, attr);
                Assert.AreEqual(1, attr.Emoticons.Count);
                Assert.AreEqual("boom", attr.Emoticons[0]);
            }

            // Emoticon contains alphanumeric characters.
            {
                string message = "(bleh23) is missing...";
                target.ParseEmoticons(message, attr);
                Assert.AreEqual(1, attr.Emoticons.Count);
                Assert.AreEqual("bleh23", attr.Emoticons[0]);

                message = "(bleh23#@$#) is missing...";
                target.ParseEmoticons(message, attr);
                Assert.AreEqual(0, attr.Emoticons.Count);
            }
        }

        [TestMethod]
        public void ParseLinksTest()
        {
            var target = new MessageService();
            var attr = new ChatMessageAttributes();

            // One link
            {
                string message = "https://www.nbc.com A quick brown fox jumped over the lazy dog.";
                target.ParseLinks(message, attr);
                Assert.AreEqual(1, attr.Links.Count);
                Assert.IsTrue(attr.Links[0].Url.Length > 0);
                Assert.IsTrue(attr.Links[0].Title.Length > 0);
            }

            // Multiple links
            {
                string message = "https://www.nbc.com A quick brown fox jumped over the lazy dog http://google.com.";
                target.ParseLinks(message, attr);
                Assert.AreEqual(2, attr.Links.Count);
                Assert.IsTrue(attr.Links[0].Url.Length > 0);
                Assert.IsTrue(attr.Links[0].Title.Length > 0);
                Assert.IsTrue(attr.Links[1].Url.Length > 0);
                Assert.IsTrue(attr.Links[1].Title.Length > 0);
            }
        }
    }
}
