﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HipChatService.Common
{
    /// <summary>
    /// Contextual links within chat messages.
    /// </summary>
    [DataContract(Name = "Link")]
    public sealed class ChatMessageLink
    {
        /// <summary>
        /// Url as given in the chat message.
        /// </summary>
        [DataMember(Name = "url")]
        public string Url { get; set; } = "";

        /// <summary>
        /// Title of the page that the url points to.
        /// </summary>
        [DataMember(Name = "title")]
        public string Title { get; set; } = "";

        public ChatMessageLink()
        {
        }
    }
}
