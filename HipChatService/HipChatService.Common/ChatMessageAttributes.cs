﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace HipChatService.Common
{
    /// <summary>
    /// Attributes of a ChatMessage such as mentions, emoticons and links.
    /// </summary>
    [DataContract(Name="ChatMessageAttributes")]
    public sealed class ChatMessageAttributes
    {
        /// <summary>
        /// Users mentioned in the chat message. 
        /// See mentions at https://confluence.atlassian.com/hipchat/get-teammates-attention-744328217.html.
        /// </summary>
        [DataMember(Name = "mentions")]
        public List<string> Mentions { get; private set; } = new List<string>();

        /// <summary>
        /// Emoticons used in the chat message. 
        /// See emoticons at https://www.hipchat.com/emoticons.
        /// </summary>
        [DataMember(Name = "emoticons")]
        public List<string> Emoticons { get; private set; } = new List<string>();

        /// <summary>
        /// Urls found within chat message.
        /// </summary>
        [DataMember(Name = "links")]
        public List<ChatMessageLink> Links { get; set; } = new List<ChatMessageLink>();

        /// <summary>
        /// Initializes a new instance of ChatMessageAttributes.
        /// </summary>
        public ChatMessageAttributes()
        {
        }
    }
}
