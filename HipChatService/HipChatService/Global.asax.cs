﻿using System;
using System.ServiceModel.Activation;
using System.Web.Routing;

namespace HipChatService
{
    public class Global : System.Web.HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();
        }

        private void RegisterRoutes()
        {
            RouteTable.Routes.Add(new ServiceRoute("", new WebServiceHostFactory(), typeof(MessageService)));
        }
    }
}