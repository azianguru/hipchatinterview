﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text.RegularExpressions;
using HipChatService.Common;

namespace HipChatService
{
    /// <summary>
    /// Implements the HipChat messages API to get contextual information about chat messages.
    /// </summary>
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.Single)]
    public class MessageService
    {
        /// <summary>
        /// Regex to match mentions in chat messages. Matches any '@' followed by one or more word characters 
        /// and ending in a non-word character. Match returns only the username within.
        /// </summary>
        private static readonly Regex MentionsRegex = new Regex(@"(?<=@)\w+(?=\b)", RegexOptions.Compiled);

        /// <summary>
        /// Regex to match emoticons in chat messages. Matches any '(' followed by alphanumeric characters of length 15 or less
        /// and ending in ')'. Match only returns the alphanumeric text within the parentheses.
        /// </summary>
        private static readonly Regex EmoticonsRegex = new Regex(@"(?<=\()[a-zA-Z0-9]{1,15}(?=\))", RegexOptions.Compiled);

        private static readonly Regex UrlRegex = new Regex(@"(http(s)*[^\s]*)\s", RegexOptions.Compiled);

        /// <summary>
        /// Responds with "Ok" to indicate service is up.
        /// </summary>
        /// <returns></returns>
        [WebGet(UriTemplate = "messages/keepalive")]
        public string Keepalive()
        {
            return "Ok";
        }

        /// <summary>
        /// Get contextual information about given chat message such as mentions, emoticons and links.
        /// </summary>
        /// <param name="message">Chat message to parse. May be empty.</param>
        /// <returns>Contextual information parsed from given chat message. Empty body for empty message.</returns>
        [WebInvoke(
            UriTemplate = "messages",
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        public ChatMessageAttributes GetContext(string message)
        {
            // TODO: Santize user input
            // TODO: Add authn/authz
            // TODO: implement tests with all contextual elements
            try
            {
                var msgAttributes = new ChatMessageAttributes();
                ParseMentions(message, msgAttributes);
                ParseEmoticons(message, msgAttributes);
                ParseLinks(message, msgAttributes);

                return msgAttributes;
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Parses any mentions from the given message and sets the mentions collection of <paramref name="msgAttributes"/>.
        /// </summary>
        /// <param name="message">Message to parse.</param>
        /// <param name="msgAttributes">ChatMessageAttributes to update with parsed mentions.</param>
        /// <remarks>Mentions of msgAttributes is cleared even if no mentions are found in message.</remarks>
        internal void ParseMentions(string message, ChatMessageAttributes msgAttributes)
        {
            msgAttributes.Mentions.Clear();

            // Order of mentions in message is preserved.
            MatchCollection matches = MentionsRegex.Matches(message);
            foreach (Match match in matches)
            {
                msgAttributes.Mentions.Add(match.Value);
            }

            // TODO: implement support for @here and @all
        }

        /// <summary>
        /// Parses any emoticons from the given message and sets the Emoticons collection of <paramref name="msgAttributes"/>.
        /// </summary>
        /// <param name="message">Message to parse.</param>
        /// <param name="msgAttributes">ChatMessageAttributes to update with parsed emoticons.</param>
        /// <remarks>Emoticons of msgAttributes is cleared even if no mentions are found in message.</remarks>
        internal void ParseEmoticons(string message, ChatMessageAttributes msgAttributes)
        {
            msgAttributes.Emoticons.Clear();

            // Order of mentions in message is preserved.
            MatchCollection matches = EmoticonsRegex.Matches(message);
            foreach (Match match in matches)
            {
                msgAttributes.Emoticons.Add(match.Value);
            }
        }

        /// <summary>
        /// Parses any links from the given message and sets the Links collection of <paramref name="msgAttributes"/>.
        /// </summary>
        /// <param name="message">Message to parse.</param>
        /// <param name="msgAttributes">ChatMessageAttributes to update with parsed links.</param>
        /// <remarks>Links of msgAttributes is cleared even if no links are found in message.</remarks>
        internal void ParseLinks(string message, ChatMessageAttributes msgAttributes)
        {
            msgAttributes.Links.Clear();

            List<ChatMessageLink> links = new List<ChatMessageLink>();

            // 1. Parse Urls
            MatchCollection matches = UrlRegex.Matches(message);
            foreach (Match match in matches)
            {
                links.Add(new ChatMessageLink() { Url = match.Value});
            }

            // 2. Load Url
            // 3. Parse titles
            foreach (var chatMessageLink in links)
            {
                using (WebClient c = new WebClient())
                {
                    string resp = c.DownloadString(chatMessageLink.Url);
                    var doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(resp);
                    var titleNode = doc.DocumentNode.Element("html").Element("head").Element("title");
                    if (titleNode != null)
                    {
                        chatMessageLink.Title = titleNode.InnerText;
                    }
                    else
                    {
                        chatMessageLink.Title = string.Empty;
                    }
                }
            }

            msgAttributes.Links = links;
        }
    }
}
